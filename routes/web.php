<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/menu/insert', 'MenuController@insert');
Route::post('/menu/insert', 'MenuController@insert')->name('insert');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/menu/min', 'MenuController@minPrice')->name('minPrice');
Route::post('/menu/max', 'MenuController@maxPrice')->name('maxPrice');
Route::get('/test', 'MenuController@testF')->name('test');
Route::get('/menu', 'MenuController@menu');
Route::post('/menu', 'MenuController@menu')->name('menu');
Route::post('/menu/display', 'MenuController@displayDish');
