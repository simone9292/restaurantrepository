<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/displayDish.css">

</head>
</head>
    <h1 style="text-align: center">Dettagli Piatto</h1>
    <div id="whole">
        <div style="text-align: center" class="well">
            <h3>{{$dish->name}}</h3>
            <h4>Prezzo: {{$dish->price}}€</h4>
            <h4>Ordinazioni Totali: {{$total}}</h4>
            <h3>Persone che hanno ordinato questo piatto almeno una volta:</h3>
            @foreach($ords as $ord)
                <h4>{{$ord->name}}</h4>
            @endforeach
        </div>
        <div id="button-container">
            <div id="button-container-butt" class="btn">
                <form method="post" action="{{ $page }}">
                    {{ csrf_field() }}
                    <button id="buttonz" type="submit">Torna al Menu</button>
                </form>
            </div>
        </div>
    </div>
</html>
