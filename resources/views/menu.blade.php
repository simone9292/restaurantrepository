<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/menu.css">
</head>
</head>
<body>
    <h1 style="text-align: center">Menu</h1>
    <h5 style="text-align: center">Pagina {{$list->currentPage()}}</h5>
    @if(count($list)>=1)
        <div id="whole">
            <form name="prev_page_form_name" id="prev_page_form_id"  method="post" action="{{$list->previousPageUrl()}}">
                {{ csrf_field() }}
                <button id="previous" type="submit" style="float: left;margin-top: -30px">Prec</button>
            </form>
                <form name="next_page_form_name" id="next_page_form_id" action="{{$list->nextPageUrl()}}" method="post">
                    {{ csrf_field() }}
                    <button id="previous" type="submit" style="float: right;margin-top: -30px">Succ</button>
                </form>
            @foreach($list as $el)
                <div style="text-align: center" class="well">
                    <form action="{{ url("menu/display") }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" id="element_id_id" name="element_id_name" value={{$el->id}}>
                        <input type="hidden" id="current_page_id" name="current_page_name" value={{ $list->url($list->currentPage()) }}>
                        <button type="submit" class="btn-link"><h3>{{$el->name}}</h3></button>
                        <h4>Prezzo: {{$el->price}}€</h4>
                    </form>

                </div>
            @endforeach
            <br style="clear: both;" />
            <div id="button-container">
                <div id="button-container-butt" class="btn" style="float: left">
                    <form method="post" action="{{ url("menu/min") }}">
                        {{ csrf_field() }}
                        <input type="hidden" id="current_page_id" name="current_page_name" value={{ $list->url($list->currentPage()) }}>
                        <button id="buttonz" type="submit" >Piatto più Economico</button></form>
                </div>
                <div id="button-container-butt" class="btn">
                    <form method="post" action="{{ url("menu/max") }}">
                        {{ csrf_field() }}
                        <input type="hidden" id="current_page_id" name="current_page_name" value={{ $list->url($list->currentPage()) }}>
                        <button id="buttonz" type="submit" >Piatto più Costoso</button>
                    </form>
                </div>
                <br style="clear: both;" />
                <div id="button-container-butt2" class="btn">
                    <form method="get" action="{{ url("menu/insert") }}">
                        {{ csrf_field() }}
                        <input type="hidden" id="current_page_id" name="current_page_name" value={{ $list->url($list->currentPage()) }}>
                        <button id="buttonz" type="submit" >Aggiungi Piatto</button></form>
                </div>
            </div>
        </div>
    @else
        <p>Non ci sono piatti nel menu</p>
    @endif
</body>
</html>
