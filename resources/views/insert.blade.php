<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/insert.css">
</head>
<script>

    function passValues() {
        var x1 = document.getElementById("visible_name_id").value;
        var x2 = document.getElementById("visible_price_id").value;
        //alert("Visible: "+x1+"\nHidden: "+x2);

        if(x1 == ""){
            alert("Inserire un nome!");
            return false;
        }
        if(x2 == ""){
            alert("Inserire un prezzo!");
            return false;
        }
        if(isNaN(x2)){
            alert("Il prezzo non è valido!");
            return false;
        }


        document.getElementById("hidden_name_id").value = x1;
        document.getElementById("hidden_price_id").value = x2;
    }
</script>
</head>
<body>
<h1 style="text-align: center">Iserimento Nuovo Piatto</h1>
    <div id="whole">
            <div style="text-align: center" class="well">
                    <h3>Nome</h3>
                    <input type="text" id="visible_name_id" name="visible_name_name">
                    <h3>Prezzo</h3>
                    <input type="text" id="visible_price_id" style="margin-bottom: 6%">
                    @if($inserted)
                        <h3>Piatto inserito!</h3>
                    @endif
            </div>
        <div id="button-container">

            <div id="button-container-butt" class="btn">
                <form name="hidden_values_form" method="post" onsubmit="return passValues()" >
                    {{ csrf_field() }}
                    <button id="buttonz" type="submit" >Inserisci</button>
                </form>
            </div>
            <div id="button-container-butt" class="btn" style="float: left">
                <form method="get" action="{{ $page }}">
                    {{ csrf_field() }}
                    <button id="buttonz" type="submit" >Torna al Menu</button></form>
            </div>
            <br style="clear: both;" />
            </div>
        </div>
    </div>
</body>

</html>
