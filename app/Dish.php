<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Dish extends Model
{
    public function customers()
    {
        return $this->belongsToMany('App\Customer','dish_customer');
    }
}
