<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    public function dishes()
    {
        return $this->belongsToMany('App\Dish','dish_customer');
    }
}
