<?php

namespace App\Http\Controllers;

use App\Flight;
use App\User;
use App\Dish;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class MenuController extends Controller
{
    public function minPrice(Request $request)
    {
        $minDish = Dish::where('price',Dish::min('price'))->first();
        $peopleOrds = $minDish->customers;
        $totalPeople = count($peopleOrds);
        $peopleOrds = $peopleOrds->unique();

        return view('displayDish', ['dish' => $minDish, 'ords' => $peopleOrds, 'total' => $totalPeople, 'page' => $request->current_page_name]);
    }

    public function maxPrice(Request $request)
    {
        $maxDish = Dish::where('price',Dish::max('price'))->first();
        $peopleOrds = $maxDish->customers;
        $totalPeople = count($peopleOrds);

        return view('displayDish', ['dish' => $maxDish, 'ords' => $peopleOrds, 'total' => $totalPeople, 'page' => $request->current_page_name]);
    }

    public function menu(Request $request)
    {
        $dishes = Dish::orderBy('name')->paginate(5);
        return view('menu', ['list' => $dishes]);
    }

    public function insert(Request $request)
    {
        $name = $request->hidden_name_name;
        $price = $request->hidden_price_name;

        if($name!=null && $price!=null){
            $dish = new Dish();
            $dish->name = $name;
            $dish->price = $price;
            $dish->save();
            return view('insert', ['inserted' => true, 'page' => $request->current_page_name]);
        }
        return view('insert', ['inserted' => false, 'page' => $request->current_page_name]);
    }

    public function displayDish(Request $request){
        $dish = Dish::find($request->element_id_name);
        $ords = $dish->customers;
        $ords = collect($ords);
        $total = count($ords);
        $ords = $ords->unique()->values()->all();
        return view('displayDish', ['dish' => $dish, 'ords' => $ords, 'total' => $total, 'page' => $request->current_page_name]);
    }

    /*
    public function testF(){
        $flights = Flight::all();
        $K = "";
        foreach ($flights as $flight){
            $K .= "VOLO " . $flight->testC . '\n';
        }
        $flightT = new Flight;
        $flightT->duration = 222;
        $flightT->oil = 666;
        $flightT->testC = 2155;
        $flightT->save();
        return view('test', ['test' => $K]);
    }
    */
}
